/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.controller;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.sessionservice.DepartmentService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author avincze
 */
@Named
@ViewScoped
public class DepartmentListController implements Serializable{

    @EJB
    private DepartmentService departmentService;
    
    
    private List<Department> departmentList;


    public DepartmentListController() {
       departmentList = new ArrayList<>();
    }

    @PostConstruct
    protected void init(){
        departmentList.addAll(departmentService.getAll());
    }
    
    
    
    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<Department> departmentList) {
        this.departmentList = departmentList;
    }
    
    
    
    
}
