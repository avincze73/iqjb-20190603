/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.controller;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.sessionservice.DepartmentService;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author avincze
 */
@Named
@ViewScoped
public class AddDepartmentController implements Serializable{

    @EJB
    private DepartmentService departmentService;
    
    
    
    private String name;
  

    public AddDepartmentController() {
    
    }
    
    
    public String save(){
        Department department = new Department();
        department.setName(name);
        departmentService.add(department);
        return "departmentList.jsf";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
    
    
}
