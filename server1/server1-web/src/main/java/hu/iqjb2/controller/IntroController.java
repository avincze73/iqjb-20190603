/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.controller;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author avincze
 */
@Named
@ViewScoped
public class IntroController implements Serializable{
    
    private String txt1;
    private String txt2;

    public IntroController() {
        this.txt1 = "txt1";
        this.txt2 = "txt2";
        
        
    }
    
    
    public void changeValue(){
        this.txt2 = " hello " + this.txt1;
    }
    
    

    public String getTxt1() {
        return txt1;
    }

    public void setTxt1(String txt1) {
        this.txt1 = txt1;
    }

    public String getTxt2() {
        return txt2;
    }

    public void setTxt2(String txt2) {
        this.txt2 = txt2;
    }
    
    
    
}
