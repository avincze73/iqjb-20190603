/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.message;

import java.io.Serializable;

/**
 *
 * @author avincze
 */
public class AddDepartmentRequest implements Serializable{
    
    private String departmentName;

    public AddDepartmentRequest(String departmentName) {
        this.departmentName = departmentName;
    }

    public AddDepartmentRequest() {
    }
    
    
    

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    
    
}
