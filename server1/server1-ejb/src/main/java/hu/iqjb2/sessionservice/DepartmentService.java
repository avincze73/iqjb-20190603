/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.sessionservice;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.exception.IqjbException;
import hu.iqjb2.interceptor.LoggingTimeInterceptor;
import hu.iqjb2.remote.intf.DepartmentServiceInterface;
import hu.iqjb2.repository.DepartmentRepository1;
import hu.iqjb2.repository.DepartmentRepository2;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
@Remote(DepartmentServiceInterface.class)
 @Interceptors(LoggingTimeInterceptor.class)
public class DepartmentService implements DepartmentServiceInterface {

    @EJB
    private DepartmentRepository2 departmentRepository2;

    @EJB
    private DepartmentRepository1 departmentRepository1;

    @Resource
    private SessionContext context;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void add(Department department) {

        departmentRepository1.add(department);
        try {
            //throw new IqjbException();
            departmentRepository2.add(department);
        } catch (IqjbException ex) {
            Logger.getLogger(DepartmentService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Department> getAll() {
        ArrayList<Department> result = new ArrayList<>();
        result.addAll(departmentRepository1.getAll());
        result.addAll(departmentRepository2.getAll());
        return result;
    }

}
