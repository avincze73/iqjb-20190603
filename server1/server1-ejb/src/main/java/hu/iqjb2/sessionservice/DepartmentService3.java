/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.sessionservice;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.exception.IqjbException;
import hu.iqjb2.remote.intf.DepartmentServiceInterface;
import hu.iqjb2.repository.DepartmentRepository1;
import hu.iqjb2.repository.DepartmentRepository2;
import hu.iqjb2.repository.DepartmentRepository3;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author avincze
 */
@Transactional(Transactional.TxType.REQUIRED)
@Named
public class DepartmentService3 {

    @Inject
    private DepartmentRepository3 departmentRepository3;
    
    public void add(Department department) {

        departmentRepository3.add(department);
    }

}
