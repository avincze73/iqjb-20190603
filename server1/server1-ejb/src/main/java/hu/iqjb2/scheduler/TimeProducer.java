/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.scheduler;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Schedule;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author avincze
 */
@Singleton
@LocalBean
public class TimeProducer {

    private static final Logger LOG = Logger.getLogger(TimeProducer.class.getName());
   
    @Resource(mappedName = "jms/iqjb")
    private Queue iqjb;

    @Resource(mappedName = "jms/iqjbfactory")
    private ConnectionFactory iqjbFactory;

    @Lock(LockType.WRITE)
    @Schedule(second = "0/5", minute = "*", hour = "*")
    public void createMessage() {   
        try(Connection connection = iqjbFactory.createConnection();
            Session session = connection.createSession()){
            MessageProducer messageProducer = session.createProducer(iqjb);
            messageProducer.send(session.createTextMessage(new Date().toString()));
            LOG.info("Message is sent.");
        } catch (JMSException ex) {
            Logger.getLogger(TimeProducer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
