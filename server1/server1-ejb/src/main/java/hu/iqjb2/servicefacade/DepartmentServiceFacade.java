/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.servicefacade;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.exception.IqjbException;
import hu.iqjb2.interceptor.LoggingTimeInterceptor;
import hu.iqjb2.message.AddDepartmentRequest;
import hu.iqjb2.message.AddDepartmentResponse;
import hu.iqjb2.message.GetAllDepartmentsRequest;
import hu.iqjb2.message.GetAllDepartmentsResponse;
import hu.iqjb2.remote.intf.DepartmentServiceInterface;
import hu.iqjb2.sessionservice.DepartmentService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.interceptor.Interceptors;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean

public class DepartmentServiceFacade {

    @EJB
    private DepartmentService departmentService;
    
    
    
    @Interceptors(LoggingTimeInterceptor.class)
    public AddDepartmentResponse addDepartment(AddDepartmentRequest request){
        AddDepartmentResponse response = new AddDepartmentResponse();
        Department department = new Department(request.getDepartmentName());
        
            departmentService.add(department);
          response.setResult("Department is added.");
        return response;
    }
    
    
    public GetAllDepartmentsResponse getAllDepartmentss(GetAllDepartmentsRequest request){
        GetAllDepartmentsResponse response = new GetAllDepartmentsResponse();
        response.getDepartmentList().addAll(departmentService.getAll());
        return response;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
