/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author avincze
 */
@ApplicationException(rollback = true)
public class IqjbException extends Exception {

    /**
     * Creates a new instance of <code>IqjbException</code> without detail
     * message.
     */
    public IqjbException() {
    }

    /**
     * Constructs an instance of <code>IqjbException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public IqjbException(String msg) {
        super(msg);
    }
}
