/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.producer;

import hu.iqjb2.producer.Iqjb1Database;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author avincze
 */
@Named
public class IqjbProducer {

    @Produces
    @PersistenceContext(unitName = "iqjb2aPU")
    @Iqjb1Database
    private EntityManager em;
    
    
    @Produces
    @PersistenceContext(unitName = "iqjb2bPU")
    @Iqjb2Database
    private EntityManager em2;

}
