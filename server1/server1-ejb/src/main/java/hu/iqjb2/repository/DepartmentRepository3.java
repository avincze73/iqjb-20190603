/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.repository;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.producer.Iqjb1Database;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
public class DepartmentRepository3 {

    @Inject
    @Iqjb1Database
    EntityManager em;

    public void add(Department department) {
      em.persist(department);
    }

    public List<Department> getAll() {
        return em.createQuery("select d from Department d", Department.class)
                .getResultList();
    }

}
