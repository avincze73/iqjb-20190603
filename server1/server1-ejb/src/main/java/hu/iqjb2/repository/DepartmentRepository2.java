/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.repository;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.exception.IqjbException;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
public class DepartmentRepository2 {

    @PersistenceContext(unitName = "iqjb2bPU")
    private EntityManager em;

    public void add(Department department) throws IqjbException{
        em.persist(department);
        //throw new IqjbException();
    }

    public List<Department> getAll() {
        return em.createQuery("select d from Department d", Department.class)
                .getResultList();
    }

}
