/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.interceptor;

import hu.iqjb2.remote.intf.LoggingServiceRemote;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author avincze
 */
public class LoggingTimeInterceptor {

    @EJB(lookup = "hu.iqjb2.remote.intf.LoggingServiceRemote")
    private LoggingServiceRemote loggingService;
    
    @Resource
    private SessionContext context;
    
    @AroundInvoke
    public Object executionTimeInterceptor(InvocationContext ic) throws Exception{
        Date started = new Date();
        Object ret = ic.proceed();
        Date finished = new Date();
        loggingService.save(ic.getMethod().getName() , finished.getTime() - started.getTime() + " ms", 
                context.getCallerPrincipal().getName());
        return ret;
    }
    
    
}
