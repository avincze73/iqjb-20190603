/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.cdi;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.repository.DepartmentRepository1;
import javax.inject.Inject;
import javax.transaction.Transactional;

/**
 *
 * @author avincze
 */
@Transactional(Transactional.TxType.REQUIRED)
public class DepartmentServiceCDI {
    
    @Inject
    private DepartmentRepository1 departmentRepository1;
    
    public void add(Department entity){
        departmentRepository1.add(entity);
    }
    
}
