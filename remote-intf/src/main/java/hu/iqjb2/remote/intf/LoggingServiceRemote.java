/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.remote.intf;

import javax.ejb.Remote;

/**
 *
 * @author avincze
 */
@Remote
public interface LoggingServiceRemote {
    void save(String functionName, String executionTime, String callerPrincipal);
    
}
