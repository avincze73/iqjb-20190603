/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.domain.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author avincze
 */
@Entity
public class IqjbLog extends IqjbEntity{
    
    private String functionName;
    private String executionTime;
    private String callerPrincipal;
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
   
    public IqjbLog() {
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }

    public String getCallerPrincipal() {
        return callerPrincipal;
    }

    public void setCallerPrincipal(String callerPrincipal) {
        this.callerPrincipal = callerPrincipal;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    
    
}
