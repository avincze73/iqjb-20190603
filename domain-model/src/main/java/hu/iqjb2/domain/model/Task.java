/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.domain.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author avincze
 */
@Entity
public class Task extends IqjbEntity{
    
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "PARTICIPANT_ID", referencedColumnName = "ID")
    private Employee participant;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getParticipant() {
        return participant;
    }

    public void setParticipant(Employee participant) {
        this.participant = participant;
    }
    
    
}
