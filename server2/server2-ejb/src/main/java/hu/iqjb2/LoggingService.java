/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2;

import hu.iqjb2.domain.model.IqjbLog;
import hu.iqjb2.remote.intf.LoggingServiceRemote;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author avincze
 */
@Stateless
public class LoggingService implements LoggingServiceRemote {

    @EJB
    private IqjbLogRepository iqjbLogRepository;

    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(String functionName, String executionTime, String callerPrincipal) {
        IqjbLog iqjbLog = new IqjbLog();
        iqjbLog.setCallerPrincipal(callerPrincipal);
        iqjbLog.setCreated(new Date());
        iqjbLog.setExecutionTime(executionTime);
        iqjbLog.setFunctionName(functionName);
        iqjbLogRepository.save(iqjbLog);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
