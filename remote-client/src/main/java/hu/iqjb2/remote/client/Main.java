/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.iqjb2.remote.client;

import hu.iqjb2.domain.model.Department;
import hu.iqjb2.remote.intf.DepartmentServiceInterface;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author avincze
 */
public class Main {
    
    public static void main(String[] args) {
        try {
            Context context = new InitialContext();
            DepartmentServiceInterface intf =
            (DepartmentServiceInterface) 
                    context.lookup("hu.iqjb2.remote.intf.DepartmentServiceInterface");
            
            Department department = new Department("department3");
            intf.add(department);
            
            department = new Department("department4");
            intf.add(department);
            
            department = new Department("department5");
            intf.add(department);
            
            intf.getAll().forEach(d->System.out.println(d.getName()));
        } catch (NamingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
