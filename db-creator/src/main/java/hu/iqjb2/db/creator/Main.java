/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
ezredes 13. 9 csengő
Pratte György

 */
package hu.iqjb2.db.creator;

import hu.iqjb2.domain.model.Address;
import hu.iqjb2.domain.model.Department;
import hu.iqjb2.domain.model.Employee;
import hu.iqjb2.domain.model.Project;
import hu.iqjb2.domain.model.Role;
import hu.iqjb2.domain.model.Task;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author avincze https://gitlab.com/avincze73/iqjb-20190603
 */
public class Main {
    
    
    private static void initDb(EntityManagerFactory factory){
         EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        
        Role role1 = new Role("admin");
        em.persist(role1);
        
        Role role2 = new Role("user");
        em.persist(role2);
        
        Department department1 = new Department("department1");
        em.persist(department1);
        
        Department department2 = new Department("department2");
        em.persist(department2);
        
        Employee employee = new Employee();
        employee.setFirstName("firstName1");
        employee.setLastName("lastName1");
        employee.setLoginName("loginName1");
        employee.setPassword("password1");
        employee.setTitle("title1");
        employee.setSalary(1000);
        em.persist(employee);
        employee.setAddress(new Address("1111", "city1", "street1"));
        employee.setBoss(null);
        employee.setDepartment(department1);
        employee.getRoleList().add(role1);
        employee.getRoleList().add(role2);
        Employee boss = employee;
        
        employee = new Employee();
        employee.setFirstName("firstName2");
        employee.setLastName("lastName2");
        employee.setLoginName("loginName2");
        employee.setPassword("password2");
        employee.setTitle("title2");
        employee.setSalary(2000);
        em.persist(employee);
        employee.setAddress(new Address("222", "city2", "street2"));
        employee.setBoss(boss);
        employee.setDepartment(department2);
        employee.getRoleList().add(role2);
        
        Project project1 = new Project();
        project1.setFromDate(new Date(new Date().getTime() - 100000000));
        project1.setToDate(new Date(new Date().getTime() + 100000000));
        project1.setName("project1");
        project1.setOwner(employee);
        
        Task task = new Task();
        task.setName("task1");
        task.setParticipant(boss);
        project1.getTaskList().add(task);
        em.persist(project1);
        
        em.getTransaction().commit();
        
        factory.close();
    }
    
    public static void main(String[] args) {
        initDb(Persistence.createEntityManagerFactory("iqjb2aPU"));
        initDb(Persistence.createEntityManagerFactory("iqjb2bPU"));
        Persistence.createEntityManagerFactory("iqjbLoggerPU");
    }
}